﻿CREATE TABLE [dbo].[Channel]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NULL
)
