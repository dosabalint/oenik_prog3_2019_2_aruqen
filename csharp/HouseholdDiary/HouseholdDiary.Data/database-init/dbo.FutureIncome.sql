﻿CREATE TABLE [dbo].[FurureIncome]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NULL, 
    [Ammount] INT NULL, 
    [Year] INT NULL, 
    [Month] INT NULL, 
    [Completed] BIT NULL DEFAULT 0
)
