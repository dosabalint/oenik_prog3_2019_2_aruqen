﻿CREATE TABLE [dbo].[Transaction]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NULL, 
    [Date] DATE NULL, 
    [Ammount] INT NULL, 
    [ChannelID] INT NULL, 
    [BudgetId] INT NULL,

)
