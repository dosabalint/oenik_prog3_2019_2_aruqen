﻿CREATE TABLE [dbo].[Budget]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NULL, 
    [Month] INT NULL, 
    [Year] INT NULL, 
    [Ammount] INT NULL, 
    [Remaining] INT NULL, 
    [Overspent] INT NULL
)
