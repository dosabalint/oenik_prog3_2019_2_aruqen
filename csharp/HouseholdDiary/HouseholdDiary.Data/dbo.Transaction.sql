﻿CREATE TABLE [dbo].[Transaction] (
    [Id]        INT           NOT NULL,
    [Name]      NVARCHAR (50) NULL,
    [Date]      DATE          NULL,
    [Ammount]   INT           NULL,
    [BudgetId]  INT           NULL,
    [Comment]	NVARCHAR(50)  NULL,
);

