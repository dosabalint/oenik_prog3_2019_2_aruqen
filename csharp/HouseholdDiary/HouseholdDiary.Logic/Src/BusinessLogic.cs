﻿namespace HouseholdDiary.Logic.Src
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using HouseholdDiary.Data;
    using HouseholdDiary.Repository.Src;

    public class BusinessLogic
    {
        private readonly TransactionRepository transactionRepo;

        public BusinessLogic()
        {
            this.transactionRepo = new TransactionRepository();
        }

        public List<string> GetTransactions()
        {
            return this.transactionRepo.List().Select(item => TransactionToString(item)).ToList();
        }

        private static string TransactionToString(Transaction tr)
        {
            return string.Format(
                "[{0}] {1} - {2} Ft | budgetId: {3} | comment: {4}",
                DateTime.Parse(tr.Date.ToString()).ToString("yyyy-MM-dd"),
                tr.Name,
                tr.Ammount,
                tr.BudgetId,
                tr.Comment);
        }
    }
}
