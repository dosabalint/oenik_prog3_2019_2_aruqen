﻿namespace HouseholdDiary.Repository.Src
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    interface IRepository<T>
    {
        List<T> List();

        T Create();

        T Read();

        T Update();

        void Delete();
    }
}
