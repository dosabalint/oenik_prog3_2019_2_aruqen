﻿namespace HouseholdDiary.Repository.Src
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using HouseholdDiary.Data;

    public class TransactionRepository : IRepository<Transaction>
    {
        private DatabaseEntities context;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionRepository"/> class.
        /// </summary>
        public TransactionRepository()
        {
            this.context = new DatabaseEntities();
        }

        public List<Transaction> List()
        {
            return this.context.Transactions.ToList();
        }

        public Transaction Create()
        {
            throw new NotImplementedException();
        }

        public Transaction Read()
        {
            throw new NotImplementedException();
        }

        public Transaction Update()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }
    }
}
