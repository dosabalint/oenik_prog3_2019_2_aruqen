﻿namespace HouseholdDiary
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using HouseholdDiary.Data;
    using HouseholdDiary.Logic.Src;

    internal class UserInterface
    {
        private static readonly Dictionary<string, Action<string>> Commands = new Dictionary<string, Action<string>>();

        private static readonly BusinessLogic BL = new BusinessLogic();

        delegate void CustomAction(string args = "");

        public static void Run()
        {
            LoadCommands();

            bool exit = false;

            Console.WriteLine();

            while (!exit)
            {
                Console.Write("$ ");
                string command = Console.ReadLine();
                string[] commandParts = command.Split(' ');
                if (Commands.ContainsKey(commandParts[0]))
                {
                    Commands[commandParts[0]](command.Replace(commandParts[0], ""));
                }

                switch (command)
                {
                    case "q":
                    case "exit":
                        exit = true;
                        break;
                }

                Console.WriteLine();
            }
        }

        private static void LoadCommands()
        {
            Action<string> help = (string args) =>
               {
                   string text = "Commands: " + Environment.NewLine +
                                 " budget" +
                                 Environment.NewLine +
                                 "  list:   bl" +
                                 Environment.NewLine +
                                 "  create: bc <name>;<year>;<month>;<amount>" +
                                 Environment.NewLine +
                                 "  update: bu <field>;<value>" +
                                 Environment.NewLine +
                                 "  delete: bd <id>" +
                                 Environment.NewLine +
                                 " future income" +
                                 Environment.NewLine +
                                 "  list:   fil" +
                                 Environment.NewLine +
                                 "  create: fic <name>;<year>;<month>;<amount>" +
                                 Environment.NewLine +
                                 "  update: fiu <field>;<value>" +
                                 Environment.NewLine +
                                 "  delete: fid <id>" +
                                 Environment.NewLine +
                                 " transaction: " +
                                 Environment.NewLine +
                                 "  list:   tl " +
                                 Environment.NewLine +
                                 "  create: tc <name: string>;<date: yyyy-MM-dd>;<channel_id>;<amount>;<budget_id>" +
                                 Environment.NewLine +
                                 "  update: tu <field>;<value>" +
                                 Environment.NewLine +
                                 "  delete: td <id>" +
                                 Environment.NewLine +
                                 " not crud 1" +
                                 Environment.NewLine +
                                 " not crud 2" +
                                 Environment.NewLine +
                                 " not crud 3" +
                                 Environment.NewLine +
                                 " help: help | ?" +
                                 Environment.NewLine +
                                 " exit: q" +
                                 Environment.NewLine;

                   Console.Write(text);
               };

            Commands.Add("?", help);

            Commands.Add("help", help);

            Commands.Add("tl", (string args) =>
            {
                foreach (string row in BL.GetTransactions())
                {
                    Console.WriteLine(row);
                }
            });

            // <name>;<date: yyyy-mm-dd>;<channel_id>;<amount>;<budget_id>
            Commands.Add("tc", (string argsString) =>
            {
                string[] args = argsString.Trim().Split(';');

                if (args.Length != 5)
                {
                    Console.WriteLine("Invalid args[] length.");
                    return;
                }

                string name = args[0];
                DateTime date;
                int channelId;
                int amount;
                int budgetId;

                try
                {
                    date = ParseDate(args[1]);
                    channelId = ParseInt(args[2]);
                    amount = ParseInt(args[3]);
                    budgetId = ParseInt(args[4]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }

                Console.WriteLine(name);
                Console.WriteLine(date.ToString());
                Console.WriteLine(channelId);
                Console.WriteLine(amount);
                Console.WriteLine(budgetId);

            });
        }



        static DateTime ParseDate(string sDate)
        {
            DateTime date;
            try
            {
                return DateTime.ParseExact(sDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                throw new Exception("Invalid date format.");
            }
        }

        static int ParseInt(string sInt)
        {
            if (!Regex.IsMatch(sInt, @"\d"))
            {
                throw new Exception("Invalid number format.");
            }

            return int.Parse(sInt);
        }
    }
}