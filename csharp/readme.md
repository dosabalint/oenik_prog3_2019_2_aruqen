# HouseholdDiary

## Funkciók szövegesen

A felhasználó autentikációt követően kezelheti pénzügyeit.
Felvehet anyagi lábakat. Felvehet jövőbeni bevételeket.
Logolhatja az eddigi tranzakcióit.

Ezáltal egyrészt lát egy cashflowt az adott hónapról nagyvonalakban.
Másrészt követheti eddigi pénzügyeit.

A meghatározott keretekkel számolhat előre, követheti a kereteken belüli költéseket, illetve túlköltéseket.

## Funkciók

### User related

- regisztráció
- bejelentkezés
- saját adatok szerkesztése
    - név
    - login
    - jelszó

### Anyagi lábak kezelése (channel)

- létrehozás
- törlés
    - rajta levő tranzakciók esetén hibaüzenet
- szerkesztés

### Költségkeretek kezelése (budget)

- létrehozás
- korábbi/aktuális havi budget klónozása
- törlés
    - adott havi hozzá tartozó tranzakció esetén hibaüzenet
- szerkesztés

### Jövőbeli bevételek (futureIncomes)

- létrehozás
- törlés
- szerkesztés

### Tranzakciók (transactions)

- létrehozás
- törlés
- szerkesztés
- klónozás


