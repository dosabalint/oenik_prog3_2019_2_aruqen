# HouseholdDiary

## Funkciók szövegesen

A felhasználó autentikációt követően kezelheti pénzügyeit.
Felvehet anyagi lábakat. Felvehet jövőbeni bevételeket.
Logolhatja az eddigi tranzakcióit.

Ezáltal egyrészt lát egy cashflowt az adott hónapról nagyvonalakban.
Másrészt követheti eddigi pénzügyeit.

A meghatározott keretekkel számolhat előre, követheti a kereteken belüli költéseket, illetve túlköltéseket.

## Funkciók

### User related

- create
- validate (validate & read)
    - name
- update
- delete

### Anyagi lábak kezelése (channel)

- create
- read
    - name
- update
- delete
- list

### Költségkeretek kezelése (budget)

- create
- read
    - name
    - month
    - ammount
    - remaining
    - overspending
- update
- delete
- list

### Jövőbeli bevételek (futureIncomes)

- create
- read
    - name
    - month
    - ammount
    - completed
- update
- delete
- list

### Tranzakciók (transactions)

- create
- read
    - date
    - name
    - ammount
    - budget
    - channel
- update
- delete
- list
